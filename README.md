# docker-confluence #

This is a repository containing the source files for the *docker-confluence* Docker image.

This image allows you to run an instance of [Atlassian Confluence](https://www.atlassian.com/software/confluence).

## Getting the files ##
Firstly, your host which you wish to run these images must have both Docker and Git installed.

Click the *Clone* link on the top left of this page and copy, paste, and run the command shown in the terminal window of the host. The files will be downloaded into a folder in the current directory called *docker-confluence*. Change directory inside that folder.

## Building the image ##
To build the image, run the below command.

```
#!bash
docker build -t confluence:5.6.5 .
```
This will create an image called *confluence* with the tag *5.6.5* from the Dockerfile in the current directory.
## Running a container ##
This image requires a directory on the host to be mapped to the */usr/local/confluence-data* directory inside the container. This will allow data persistence, as that folder will be used for storing uploaded files and various things that needs to persist. Most data is stored in the MSSQL database however (configured within the app).

### Auto-starting a container on boot ###
To auto-start a container at boot using Systemd, you can create a unit file on the server in the folder */etc/systemd/system*. The below file is called *confluence.service*.

```
[Unit]
Description=Atlassian Confluence docker container
After=docker.service nginx-proxy.service
Requires=docker.service nginx-proxy.service

[Service]
ExecStartPre=-/usr/bin/docker stop confluence
ExecStartPre=-/usr/bin/docker rm confluence
ExecStart=/usr/bin/docker run --rm --name confluence -e VIRTUAL_HOST=wiki,wiki.hdy44.com.au -v /data/confluence:/usr/local/confluence-data:rw -i -t confluence:5-6-5
ExecStop=/usr/bin/docker stop confluence

[Install]
WantedBy=multi-user.target
```
Some things to note:

* the unit file requires the *docker.service*, and the *nginx-proxy.service* services to be started first. You'll need to create them first.
* Docker is run with the switch *-e VIRTUAL_HOST=wiki,wiki.hdy44.com.au*, which tells the *nginx-proxy* container to set up a virtual host for this container with these host names, and point it at the web ports of this container.
* Docker is run with the switch *-e CERT_NAME=hdy-docker-144*, which tells the *nginx-proxy* container to use the files *hdy-docker-144.crt* and *hdy-docker-144.key* as the SSL certificate for this virtual host.
* Docker is run with the switch *-v /data/confluence:/usr/local/confluence-data:rw*, which maps the */data/confluence* folder on the host to the */usr/local/confluence-data* folder in the container with read/write permissions (*rw*)

Once that file is created, you can enable it to start at boot, and run it, using the below commands:

```
#!bash
sudo systemctl enable confluence.service
sudo systemctl start confluence.service
```
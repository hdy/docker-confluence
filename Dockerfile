FROM debian:wheezy

MAINTAINER "Henry Davis York" <helpdesk@hdy.com.au>

# Create the directories which will contain the Confluence files
RUN mkdir -p /usr/local/confluence /usr/local/confluence-data/
WORKDIR /usr/local/confluence

# Install Java 7 APT repositories
RUN echo "deb http://ppa.launchpad.net/webupd8team/java/ubuntu precise main" | \
        tee /etc/apt/sources.list.d/webupd8team-java.list && \
    echo "deb-src http://ppa.launchpad.net/webupd8team/java/ubuntu precise main" | \
        tee -a /etc/apt/sources.list.d/webupd8team-java.list && \
    echo oracle-java7-installer shared/accepted-oracle-license-v1-1 select true | \
        /usr/bin/debconf-set-selections && \
    apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys EEA14886

# Install Confluence prerequisites
RUN apt-get update -y && \
    apt-get install -y \
        oracle-java7-installer \
        oracle-java7-set-default \
        tar \
        wget && \
    rm -rf /var/lib/apt/lists/*

# Add functionality that allows the Kerberos plugin to work
ADD kerberos/US_export_policy.jar /usr/lib/jvm/java-7-oracle/jre/lib/security/US_export_policy.jar
ADD kerberos/local_policy.jar /usr/lib/jvm/java-7-oracle/jre/lib/security/local_policy.jar

# Download and extract Confluence 5.6.5
RUN wget http://www.atlassian.com/software/confluence/downloads/binary/atlassian-confluence-5.6.5.tar.gz \
        --no-check-certificate && \
    tar -zxvf atlassian-confluence-5.6.5.tar.gz --strip 1 && \
    rm atlassian-confluence-5.6.5.tar.gz && \
    sed -i '/^# confluence.home=*/cconfluence.home=/usr/local/confluence-data/' \
        confluence/WEB-INF/classes/confluence-init.properties

# Download the MySQL JDBC driver
WORKDIR /tmp
RUN wget http://dev.mysql.com/get/Downloads/Connector-J/mysql-connector-java-5.1.34.tar.gz \
	--no-check-certificate && \
    tar -zxvf mysql-connector-java-5.1.34.tar.gz --strip 1 && \
    rm mysql-connector-java-5.1.34.tar.gz && \
    cp mysql-connector-java-5.1.34-bin.jar /usr/local/confluence/confluence/WEB-INF/lib/mysql-connector-java-5.1.34-bin.jar
WORKDIR /usr/local/confluence

ADD custom /usr/local/confluence/confluence/custom

# By default, Confluence listens on port 8090. We can leave this as default and just use
# Docker to map the port we want it to listen on (eg for port 80, we'd use "-p 80:8090")
EXPOSE 8090

VOLUME ["/usr/local/confluence-data/"]

# Run Confluence with the -fg switch to have it run in the foreground
ENTRYPOINT ["/usr/local/confluence/bin/start-confluence.sh", "-fg"]
